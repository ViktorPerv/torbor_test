<?php

namespace app\components;

use Yii\Base\BaseObject as baseObject;

/**
 * Class diffController
 * @package app\controllers
 */
class DiffControl extends baseObject {

    private $_procDev;
    private $_amountCurrent;
    private $_amountPrev;
    private $_procDiff;

    private $_diff;

    /**
     * diffController constructor.
     * @param int $proc_dev     Допустимое отклоенение в %
     * @param int $amount       Текущая цена
     * @param int $amount_prev  Предыдущая цена
     * @param int $proc_diff    Результат в %
     * @param array $config     Свойства BaseObject
     */
    public function __construct( $proc_dev, $amount, $amount_prev = 1, $proc_diff =0, array $config = [])
    {
        $this->_procDev = abs($proc_dev);
        $this->_amountCurrent = abs($amount);
        $this->_amountPrev = abs($amount_prev);
        $this->_procDiff = abs($proc_diff);

        $this->setDiff();

        parent::__construct($config);
    }

    /**
     * @return int
     */
    private function getAmountCurrent() {
        return $this->_amountCurrent;
    }

    /**
     * @return int
     */
    private function getAmountPrev() {
        return $this->_amountPrev;
    }

    /**
     * @return int
     */
    private function getPreocDev() {
        return $this->_procDev;
    }

    /**
     * @return bool
     */
    public function diff() {

        return (boolean) ($this->getAmount() <= $this->getPreocDev());
    }


    /**
     * Вычисление отклонение в % нового числа от предыдущего
     */
    private function setDiff() {
        if ($this->getAmountCurrent() == 0 && $this->getAmountPrev() == 0) {
            $this->_diff = 0;
        }
        elseif($this->getAmountCurrent() == 0 && $this->getAmountPrev() != 0) {
            $this->_diff = 100;
        }
        else {
            if (  $this->getAmountCurrent() > $this->getAmountPrev()) {
                $this->_diff = round((($this->getAmountCurrent() - $this->getAmountPrev()) / $this->getAmountCurrent()) * 100, 1);
            }
            else {
                $this->_diff = round((($this->getAmountPrev() - $this->getAmountCurrent()) / $this->getAmountCurrent()) * 100, 0);
            }
        }

    }

    /**
     * Возвращает разницу в %
     * @return int
     */
    public function getAmount() {
        return (int) abs($this->_diff);
    }


}